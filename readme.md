# OS X Instructions


## Download:

Unzip to your machine: https://www.nasm.us/pub/nasm/releasebuilds/2.13.01/macosx/

Version is important as there are unfixed show stopper bugs in later versions of nasm


Clone:

https://gitlab.com/rnghack/nasm-tools

https://gitlab.com/rnghack/asm-lib


## Setup Instructions: 

1) Ensure each script from nasm-tools is updated to point to nasm 2.13.01
2) Add each build script in nasm-tools to your path (ca, ea, ta)


## Running

Compiling:

```ca  <dir>```


Running examples:

```ea  <dir>```


Running tests

```ta  <dir>```
